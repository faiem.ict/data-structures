#include<bits/stdc++.h>
using namespace std;

void solve(int arr[], int size)
{
    int current = arr[0];
    int count = 0;
    for(int I=0;I<size;I++)
    {
        if(current < arr[I])
        {
            cout<<current<<" "<<count<<endl;
            count = 1;
            current = arr[I];
        }
        else
        {
            count++;
        }

    }
    cout<<current<<" "<<count<<endl;
}


int main()
{
     int tc = 1;
     //cin>>tc;

     while(tc--)
     {
         int arr[] = {10,10,10,25,30,30};
         solve(arr, 6);
     }
}