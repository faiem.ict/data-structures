#include<bits/stdc++.h>
using namespace std;

int solve(int price[], int size)
{
    int profit = 0;
    for(int I=1;I<size;I++)
    {
        if(price[I] > price[I-1])
        {
            profit += (price[I] - price[I-1]);
        }
    }
    return profit;
}


int main()
{
     int tc = 1;
     //cin>>tc;

     while(tc--)
     {
         int arr[] = {1,5,3,8,12};
         int size = 5;
         int max_profit = solve(arr, size);
         cout<<max_profit<<endl;
     }
}