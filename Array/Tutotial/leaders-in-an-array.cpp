//An element is called the leader of an array 
//if there is no element greater than it on the right side.
#include<bits/stdc++.h>
using namespace std;

vector<int> solve(int arr[], int size)
{
    vector<int> output;
    int current_max = arr[size-1]-1;

    for(int I=size-1;I>=0;I--)
    {
        if(current_max < arr[I])
        {
            output.push_back(arr[I]);
            current_max = arr[I];
        }

    }

    return output;
}


int main()
{
     int tc = 1;
     //cin>>tc;

     while(tc--)
     {
         int arr[] = {7,10,4,10,6,5,2};
         int size = 7;
         vector<int> result = solve(arr, 7);
         for(int I=0;I<result.size();I++)
         {
             cout<<result[I]<<endl;
         }
         cout<<endl;
     }
}